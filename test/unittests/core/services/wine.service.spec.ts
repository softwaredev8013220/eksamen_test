import { WineService } from '../../../../src/core/services/wine.service';
import spyOn = jest.spyOn;
import { oneWineModel, wineToAdd, wineToAddNoName, wineToAddNoNameOrPrice, wineToAddNoPrice } from '../../../fakes/wine.stub';
import { Test, TestingModule } from '@nestjs/testing';
import { mockWineData, oneWine } from '../../../../test/fakes/wine.mocks';

describe('WineService', () => {
  let wineService: WineService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      controllers: [],
      providers: [
        WineService,
        {
          provide: 'IWineData',
          useValue: mockWineData,
        },
      ],
    }).compile();
    wineService = moduleRef.get<WineService>(WineService);
  });

  it('should be defined', function () {
    expect(wineService).toBeDefined();
  });

  describe('fetchAllWines', function () {
    it('should call winedb once', async () => {
      const spy = spyOn(mockWineData, 'fetchAll');
      await wineService.fetchAllWines();
      expect(spy).toHaveBeenCalledTimes(1);
    });
    it('should return an array of wines', async () => {
      const call = await wineService.fetchAllWines()
      expect(call).toBeInstanceOf(Array);
      expect(call).toEqual(
          expect.arrayContaining([expect.objectContaining(oneWine)]),
      );
    })
  });

  describe('add', () => {
    test.each([
      { testname: 'should return wine if added', input: wineToAdd, expectedResult: oneWine },
      { testname: 'should throw error if no name or price', input: wineToAddNoNameOrPrice, expectedResult: new Error('Add name and price before saving.')},
      { testname: 'should throw error if price but no name', input: wineToAddNoName, expectedResult: new Error('Add name and price before saving.')},
      { testname: 'should throw error if name but no price', input: wineToAddNoPrice, expectedResult: new Error('Add name and price before saving.')}
    ])('%s', async (testcase) => {
      const call = await wineService.addWine(testcase.input)
      expect(call).toEqual(testcase.expectedResult)
    })
    it('should call wineData once and pass a wine to wineData to save', async () => {
      const spy = spyOn(mockWineData, 'save');
      await wineService.addWine(oneWineModel);
      expect(spy).toHaveBeenCalledTimes(1);
      expect(spy).toHaveBeenCalledWith(oneWineModel);
    });
  })

  describe('GetByID', () => {
    it('Should return winemodel', async () => {
      const call = await wineService.getById("32423");
      expect(call).toEqual(oneWine)
    })
    it('should call winedata once and pass the id', async () => {
      const id = '123'
      const spy = spyOn(mockWineData, 'get')
      await wineService.getById(id)
      expect(spy).toBeCalledTimes(1)
      expect(spy).toHaveBeenCalledWith(id)
      })
  })

  afterEach(() => {
    jest.clearAllMocks();
  });
});
