import { Test, TestingModule } from '@nestjs/testing';
import spyOn = jest.spyOn;
import { HttpException, HttpStatus } from '@nestjs/common';
import { WineController } from '../../../src/controllers/wine.controller';
import { WineService } from '../../../src/core/services/wine.service';
import { mockWineService, oneWine } from '../../../test/fakes/wine.mocks';
import { wineToAdd } from '../../../test/fakes/wine.stub';

describe('WineController', () => {
  let wineController: WineController;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      controllers: [WineController],
      providers: [{ provide: WineService, useValue: mockWineService }],
    }).compile();
    wineController = moduleRef.get<WineController>(WineController);
  });

  it('should be defined', function () {
    expect(wineController).toBeDefined();
  });

  describe('/fetchAll', () => {
    it('should call service once', async () => {
      const spy = spyOn(mockWineService, 'fetchAllWines')
      await wineController.fetchAllWines()
      expect(spy).toBeCalledTimes(1)
    })
    it('should return an array of wines', async () => {
      const call = await wineController.fetchAllWines();
      expect(call).toBeInstanceOf(Array);
      expect(call).toEqual(
        expect.arrayContaining([expect.objectContaining(oneWine)]),
      );
    })
    it('should throw HttpException in case of error', async () => {
      // @ts-ignore
      mockWineService.fetchAllWines.mockRejectedValue(new HttpException('ErrorMessage'))
      await expect(wineController.fetchAllWines()).rejects.toThrow(HttpException)
    });
  });

  describe('/byId/:id', () => {
    it('should throw error if there is no id', async () => {
      const call = await wineController.fetchById('')
            expect(call).toEqual(new HttpException("Enter id.", HttpStatus.CONFLICT))
       
    })
    it('should return winedto', async () => {
      const call = await wineController.fetchById('12343')
      expect(call).toEqual(oneWine)
    })
  })

  describe('Post', () => {
    it('should call service one with a winemodel', async () => {
      const spy = spyOn(mockWineService, 'addWine')
      await wineController.save(wineToAdd)
      expect(spy).toHaveBeenCalledWith(wineToAdd)
      expect(spy).toBeCalledTimes(1)
    })
    it('should return a winedto with id', async () => {
      const call = await wineController.save(wineToAdd)
      expect(call).toEqual(oneWine)
    })
    it('should throw HttpException if rejected', async () => {
      // @ts-ignore
      mockWineService.addWine.mockRejectedValue(new HttpException('ErrorMessage'))
      await expect(wineController.save(wineToAdd)).rejects.toThrow(HttpException)
    
    })
  })

  afterEach(() => {
    jest.clearAllMocks();
  });
});
