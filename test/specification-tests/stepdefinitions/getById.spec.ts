import { binding, given, then, when } from "cucumber-tsflow";
import { WineController } from "../../../src/controllers/wine.controller";
import { oneWineModel, wines } from "../../../test/fakes/wine.stub";
import { Test, TestingModule } from "@nestjs/testing";
import { WineService } from "../../../src/core/services/wine.service";
import { HttpException, HttpStatus } from "@nestjs/common";
const assert = require('assert')

@binding()
export class ControllerSteps {
  controller: WineController;
  result;
  
//WineDb
 mockWineData = {
    get: (() => {
      return oneWineModel;
    }),
    save: ((wine) => {
      return { name: wine.name, price: wine.price, reserved: wine.reserved, stock: wine.stock };
    }),
    edit: (() => {

    }),
    fetchAll: (() => {
        return wines
    })
};

  wine: {name: string, price: number, stock: number, reserved: number}
  id;

  @given('a wine with id {string}') 
  public async setWine(id: string) {
    const moduleRef: TestingModule = await Test.createTestingModule({
        controllers: [WineController],
        providers: [
          WineService,
          {
            provide: 'IWineData', // The token used in WineService for the WineData dependency
            useValue: this.mockWineData, // Pass your mockWineData here
          },
        ],
      }).compile();
      
      this.controller = moduleRef.get<WineController>(WineController);
      this.id = id === 'null' ? null: id  
}
  
  @when('I get the wine with given Id')
  public async addWine(){
    this.result = await this.controller.fetchById(this.id)
  }

  @then('should return that wine')
  public success(){
    let expected = {
      name: 'SomeWineName',
      price: 50,
      stock: 20,
      reserved: 0,
    }
    assert.deepStrictEqual(expected, this.result)
  }

  @then('should return HttpException with message {string}')
  public error(output:string){
    assert.deepStrictEqual(new HttpException(output,HttpStatus.CONFLICT), this.result)
  }
}