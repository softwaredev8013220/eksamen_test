const { binding, given, then, when } = require('cucumber-tsflow')
import { oneWineModel, wines } from '../../fakes/wine.stub';
import { WineController } from '../../../src/controllers/wine.controller';
import { WineService } from '../../../src/core/services/wine.service';
import { Test, TestingModule } from '@nestjs/testing';
const assert = require('assert')

@binding()
export class ControllerSteps {
  controller: WineController;
  result;
  
//WineDb
 mockWineData = {
    get: (() => {
      return oneWineModel;
    }),
    save: ((wine) => {
      return { name: wine.name, price: wine.price, reserved: wine.reserved, stock: wine.stock };
    }),
    edit: (() => {

    }),
    fetchAll: (() => {
        return wines
    })
};

  wine: {name: string, price: number, stock: number, reserved: number}

  @given('a wine with name {string} and price {int} and stock {int} and reserved {int}') 
  public async setWine(name: string, price: number, stock: number, reserved: number) {
    const moduleRef: TestingModule = await Test.createTestingModule({
        controllers: [WineController],
        providers: [
          WineService,
          {
            provide: 'IWineData', // The token used in WineService for the WineData dependency
            useValue: this.mockWineData, // Pass your mockWineData here
          },
        ],
      }).compile();
      
      this.controller = moduleRef.get<WineController>(WineController);
      this.wine = { name: name, price: price, stock: stock, reserved: reserved }
  }
  
  @when('I add the wine')
  public async addWine(){
    this.result = await this.controller.save(this.wine)
  }

  @then('the wine is saved successfully')
  public success(){
    assert.deepStrictEqual(this.wine, this.result)
  }

  @then('should return {string}')
  public error(output:string){
    assert.deepStrictEqual(output, this.result)
  }
}

