Feature: Wine Management

  Scenario: Adding a new wine
    Given a wine with name "<Name>" and price <Price> and stock <Stock> and reserved <Reserved>
    When I add the wine
    Then the wine is saved successfully

    Examples:
        |Name         |Price | Stock |Reserved |
        |Chardonnay   |500   |9      |0        |
        |Chardonnay   |5000  |9      |10       |


  Scenario: Adding a new wine with no name and/or no price should fail
    Given a wine with name "<Name>" and price <Price> and stock <Stock> and reserved <Reserved>
    When I add the wine
    Then should return "<Output>"

    Examples:
        |Name         |Price | Stock |Reserved |Output              |
        |Chardonnay   |0     |9      |0        |Enter price and name| 
        |             |0     |10     |0        |Enter price and name| 
        |             |500   |10     |0        |Enter price and name|


  Scenario: Get wine by Id
    Given a wine with id "<Id>"
    When I get the wine with given Id
    Then should return that wine

    Examples:
        |Id                        |
        |6467b12f46b7d4c26e04f5ff  | 

    Scenario: Get wine by Id fail when id is null or empty
    Given a wine with id "<Id>"
    When I get the wine with given Id
    Then should return HttpException with message "<Output>"

    Examples:
        |Id   |Output   |
        |null |Enter id.|
        |     |Enter id.| 