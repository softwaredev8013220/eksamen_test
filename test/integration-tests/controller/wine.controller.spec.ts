import { Test, TestingModule } from "@nestjs/testing";
import { WineController } from "../../../src/controllers/wine.controller";
import { WineService } from "../../../src/core/services/wine.service";
import { mockWineData, oneWine } from "../../../test/fakes/wine.mocks";
import { HttpException, HttpStatus } from "@nestjs/common";

describe('WineService', () => {
    let wineController: WineController;
    
    beforeEach(async () => {
        const moduleRef: TestingModule = await Test.createTestingModule({
            controllers: [ WineController ],
            providers: [
                WineService,
                {
                    provide: 'IWineData',
                    useValue: mockWineData,
                },
            ],
        }).compile();
        wineController = moduleRef.get<WineController>(WineController);
    });

    describe('/fetchAll', () => {
        it('should return all wines', async () => {
            const result = await wineController.fetchAllWines()
            expect(result).toEqual(
                expect.arrayContaining([expect.objectContaining(oneWine)])
            )
        })
        it('should throw HttpException if rejected', async () => {
            // @ts-ignore
            mockWineData.fetchAll.mockRejectedValue(new Error('ErrorMessage'))
            await expect(wineController.fetchAllWines()).rejects.toThrow(HttpException)
        })
    })

    describe('/byId/:id', () => {
        test.each([
            { testname: 'should return HttpException id id is empty', input: '', expectedResult: new HttpException('Enter id.', HttpStatus.CONFLICT) },
            { testname: 'should return HttpException id id is null', input: null, expectedResult: new HttpException('Enter id.', HttpStatus.CONFLICT) },
            { testname: 'should return wine if id', input: '1234', expectedResult: oneWine },
        ])('%s', async (testcase) => {
            const call = await wineController.fetchById(testcase.input)
            expect(call).toEqual(testcase.expectedResult)
        })
    })

    afterAll(async () => {
        jest.clearAllMocks();
    })
});