import { oneWineModel, wines } from "./wine.stub";
import {jest} from '@jest/globals'

//WineDb
export const mockWineData = {
    get: jest.fn(() => {
      return oneWine;
    }),
    save: jest.fn(() => {
      return oneWineModel;
    }),
    edit: jest.fn(),
    fetchAll: jest.fn(() => {
        return listOfwines
    })
};

//WineService
export const mockWineService = {
    fetchAllWines: jest.fn(() => {
        return wines;
    }),
    addWine: jest.fn(() => {
        return oneWineModel;
    }),
    getById: jest.fn(() => {
        return oneWineModel;
    })
}

//One wine to assert against
export const oneWine = {
    name: expect.any(String),
    price: expect.any(Number),
    stock: expect.any(Number),
    reserved: expect.any(Number),
  };

//List of wines to assert against
export const listOfwines = [
    { _id: expect.any(String),
    name: expect.any(String),
    price: expect.any(Number),
    stock: expect.any(Number),
    reserved: expect.any(Number) },
  ];