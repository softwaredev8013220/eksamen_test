//List of wines (can be used for WineDto, WineModel, Schema)
export const wines = [
    {
      _id: '6467b12f46b7d4c26e04f5ff',
      name: 'SomeWineName',
      price: 50,
      stock: 20,
      reserved: 0,
    },
    {
      _id: '6467b2559545073eb56ff90e',
      name: 'SomeWineName',
      price: 50,
      stock: 20,
      reserved: 0,
    },
    {
      _id: '646926e560ab88a36919e503',
      name: 'SomeWineName',
      price: 50,
      stock: 20,
      reserved: 0,
    },
    {
      _id: '64692b09f20cc576a746c8a2',
      name: 'SomeWineName',
      price: 50,
      stock: 20,
      reserved: 0,
    },
  ];
  
// OneWineModel
export const oneWineModel = {
    _id: '12345678',
    name: 'SomeWineName',
    price: 50,
    stock: 20,
    reserved: 0,
};

export const wineToAdd = {
    name: 'someWineName',
    price: 500,
    stock: 7,
    reserved: 0
}

export const wineToAddNoNameOrPrice = {
  name: '',
  price: 0,
  stock: 7,
  reserved: 0
}

export const wineToAddNoName = {
  name: '',
  price: 500,
  stock: 7,
  reserved: 0
}

export const wineToAddNoPrice = {
  name: 'ExampleName',
  price: 0,
  stock: 7,
  reserved: 0
}