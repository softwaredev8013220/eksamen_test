import { Inject } from "@nestjs/common";
import { IWineData } from "../data-interfaces/wine-data.interface";
import { IWineModel } from "../models/wine.model";

export class WineService {
    constructor(@Inject('IWineData') private readonly wineData: IWineData) {}
  
    async fetchAllWines() {
      return await this.wineData.fetchAll();
    }
  
    async addWine(wine: IWineModel) {
      if(wine.name == '' || wine.price == 0 || wine.price == null){
        return new Error('Add name and price before saving.')
      }
      const winetoreturn = await this.wineData.save(wine);
      return {
        name: winetoreturn.name,
        price: winetoreturn.price,
        stock: winetoreturn.stock,
        reserved: winetoreturn.reserved
      } as IWineModel
    }
  
    async getById(id: string) {
      return await this.wineData.get(id);
    }
  }
  
  