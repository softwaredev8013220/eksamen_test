import { IWineModel } from "../models/wine.model";

export interface IWineData {
    fetchAll(): Promise<IWineModel[]>;
    save(wine: IWineModel);
    get(productId: string): Promise<IWineModel>;
    edit(product: IWineModel);
  }
  