
export interface IWineModel {
    _id?: string;
    name: string;
    price: number;
    stock: number;
    reserved: number;
  }
  
  