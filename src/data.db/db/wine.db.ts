import { HttpException, Injectable } from "@nestjs/common";
import { IWineData } from "src/core/data-interfaces/wine-data.interface";
import { Wine, WineDocument, convertToModel } from "../schemas/wine.schema";
import { IWineModel } from "src/core/models/wine.model";
import { Model } from 'mongoose';
import { InjectModel } from "@nestjs/mongoose";

@Injectable()
export class WineDb implements IWineData {
  constructor(@InjectModel(Wine.name) private wineModel: Model<WineDocument>) {}

  async fetchAll(): Promise<IWineModel[]> {
    const fetchedWines = await this.wineModel.find();
    return fetchedWines.map((wine) => convertToModel(wine));
  }

  //The save method needs to 
  async save(wine: IWineModel) {
    const created = await this.wineModel.create({
      name: wine.name,
      price: wine.price,
      stock: wine.stock,
      reserved: wine.reserved,
    });
    return convertToModel(created);
  }

  async get(productId: string): Promise<IWineModel> {
    const fetchedWine = await this.wineModel.findOne({ _id: productId });
    return convertToModel(fetchedWine);
  }

  async edit(product: IWineModel) {
    console.log('edit db reserved and stock:', product.reserved, product.stock)
    const updatedWine = await this.wineModel.findOneAndUpdate(
      { _id: product._id },
      {
        $set: {
          stock: product.stock,
          reserved: product.reserved,
        },
      },
    );
    if (!updatedWine) {
      throw new HttpException(
        { message: 'Something went wrong with updating the wine stock' },
        500,
      );
    }
    return convertToModel(updatedWine);
  }
}

