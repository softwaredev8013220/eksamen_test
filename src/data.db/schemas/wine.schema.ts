import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { IWineModel } from 'src/core/models/wine.model';

export type WineDocument = Wine & Document;

@Schema({ collection: 'wine' })
export class Wine {
  _id?: string;
  @Prop({ type: String })
  name: string;
  @Prop({ type: Number })
  price: number;
  @Prop({ type: Number })
  stock: number;
  @Prop({ type: Number })
  reserved: number;
}

export const convertToModel = (wine: Wine): IWineModel => {
  return <IWineModel>{
    _id: wine._id.toString(),
    name: wine.name,
    price: wine.price,
    stock: wine.stock,
    reserved: wine.reserved,
  };
};

export const WineSchema = SchemaFactory.createForClass(Wine);
