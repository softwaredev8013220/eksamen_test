import { Module } from "@nestjs/common";
import { WineController } from "./controllers/wine.controller";
import { WineService } from "./core/services/wine.service";
import { WineDb } from "./data.db/db/wine.db";
import { Wine, WineSchema } from "./data.db/schemas/wine.schema";
import { MongooseModule } from "@nestjs/mongoose";
import { ConfigModule } from '@nestjs/config';
import { HttpModule } from '@nestjs/axios';
import { MongoMemoryServer } from "mongodb-memory-server";

@Module({
    imports: [
      ConfigModule.forRoot({ envFilePath: '.env' }),
      HttpModule,
      MongooseModule.forFeature([{ name: Wine.name, schema: WineSchema }]),
      MongooseModule.forRootAsync({
        useFactory: async () => ({
          uri: process.env.NODE_ENV !== 'test'
            ? (process.env.MONGO_URI as string)
            : (await MongoMemoryServer.create()).getUri
        }),
      }),
    ],
    controllers: [WineController],
    providers: [
      WineService,
      { provide: 'IWineData', useClass: WineDb },
    ],
  })
  export class AppModule {}
  