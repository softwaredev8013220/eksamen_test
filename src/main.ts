import { AppModule } from "./app.module";
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';

async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(AppModule);
    await app.listen(process.env.PORT || '3000', () => {
      console.log(`Running ${process.env.SERVICE_NAME}`)
      console.log(`App online @${process.env.PORT}`)
    });
  }
  bootstrap();
  