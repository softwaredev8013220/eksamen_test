import { IsEnum, IsNumber, IsOptional, IsString } from 'class-validator';
import { IWineModel } from '../../core/models/wine.model';



export class CreateWineDto {
    @IsString()
    name: string;
    @IsNumber()
    price: number;
    @IsNumber()
    stock: number;
    @IsNumber()
    reserved: number;
  }
  
  export class WineDto {
    @IsOptional()
    @IsString()
    _id?: string;
    @IsString()
    name: string;
    @IsNumber()
    price: number;
    @IsNumber()
    stock: number;
    @IsNumber()
    reserved: number;
  }
  
  export const convertToCreateWineModel = (wine: CreateWineDto): IWineModel => {
    return <IWineModel>{
      name: wine.name,
      price: wine.price,
      stock: wine.stock,
      reserved: wine.reserved,
    };
  };
  
  export const convertToWineDto = (wine: IWineModel | any): WineDto => {
    return <WineDto>{
      name: wine.name,
      price: wine.price,
      stock: wine.stock,
      reserved: wine.reserved,
    };
  };
  