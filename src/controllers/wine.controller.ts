import {
    ClassSerializerInterceptor,
    Controller,
    Get,
    HttpException,
    Post,
    UseInterceptors,
    Body, Param, HttpStatus
  } from "@nestjs/common";
  import { WineService } from '../core/services/wine.service';
  import {
    convertToWineDto,
    convertToCreateWineModel,
    CreateWineDto,
    WineDto,
  } from './dtos/wine.dto';
  
  @Controller('wine')
  @UseInterceptors(ClassSerializerInterceptor)
  export class WineController {
    constructor(private readonly wineService: WineService) {}
  
    @Get('/fetchAll')
    async fetchAllWines(): Promise<WineDto[]> {
      try {
        const wines = await this.wineService.fetchAllWines();
        const dtos: WineDto[] = [];
        wines.map((result) => {
          dtos.push(convertToWineDto(result));
        });
        return dtos;
      } catch (e) {
        throw new HttpException({ message: 'Something went wrong' }, e.status);
      }
    }
  
    @Get('/byId/:id')
    async fetchById(@Param('id') id: string) {
      try {
        if (id === '' || id === null ){ return new HttpException('Enter id.', HttpStatus.CONFLICT) }
        const wine = await this.wineService.getById(id);
        return convertToWineDto(wine);
      } catch (e) {
        throw new HttpException({ message: e.message }, e.status);
      }
    }
  
    @Post()
    async save(@Body() createWine: CreateWineDto) {
      try {
        if (createWine.name == '' || createWine.price == 0) {
          return 'Enter price and name'
        }
        const wine: CreateWineDto = createWine;
        const saved = await this.wineService.addWine(
          convertToCreateWineModel(wine),
        );
        return convertToWineDto(saved);
      } catch (e) {
        throw new HttpException({ message: e.message }, e.status);
      }
    }
  }
  