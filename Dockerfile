# Stage 1: Build the application
FROM node:18.18.1 AS builder

USER node
WORKDIR /home/node

# Copy package files and install dependencies
COPY package*.json ./
COPY tsconfig*.json ./
COPY cucumber.json ./
COPY .env ./
RUN npm ci

# Copy source code
COPY src/ ./src

# Copy test project
COPY test/ ./test

# Build the application
RUN npm run build

# Stage 2: Create a lightweight production image
FROM node:18.18.1 AS production

USER node
WORKDIR /home/node

# Copy package files and dependencies from builder stage
COPY --from=builder /home/node/package*.json ./
COPY --from=builder /home/node/node_modules ./node_modules
COPY --from=builder /home/node/dist ./dist
COPY --from=builder /home/node/.env ./

# Expose the port (if needed)
# EXPOSE 3000

# Command to start the application
CMD ["node", "dist/main.ts"]